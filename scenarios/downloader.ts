/**
 * we are using the proxy pattern in this cache for caching, access control and logging.
 * These are usually the most popular scenarios to use such pattern
 */

interface Subject {
  request(url: string): void;
}

class Downloader implements Subject {
  public request(url: string): void {
    console.log(`[Downloader] #request: ${url}`, url);
  }
}

/**
 * the AdvancedDownloader (using proxy pattern) has an interface identical to the Downloader.
 */
class AdvancedDownloader implements Subject {
  private downloader: Downloader;
  private url: string = "";
  private cachedUrls: string[] = [];

  constructor(downloader: Downloader) {
    this.downloader = downloader;
  }

  public request(url: string): void {
    const isCached = this.cachedUrls.includes(url);
    if (isCached) {
      console.log(`[AdvancedDownloader] #isCached: ${url} is cached. Skipping`);
      return;
    }
    if (this.checkAccess()) {
      this.url = url;
      this.downloader.request(url);
      this.cachedUrls.push(url);
      this.logAccess();
    }
  }

  private checkAccess(): boolean {
    console.log(
      "[AdvancedDownloader] #checkAccess: user has access to resource. continueing..."
    );
    return true;
  }

  private logAccess(): void {
    console.log(`[AdvancedDownloader] #logAccess: call to ${this.url}`);
  }
}

export default function SetupDownloaderScenario() {
  const downloader = new Downloader();
  downloader.request("https://google.com");

  const advancedDownloader = new AdvancedDownloader(downloader);
  advancedDownloader.request("https://google.com");
  advancedDownloader.request("https://google.pl");
  advancedDownloader.request("https://google.com");
}
