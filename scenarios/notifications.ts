export class Subject {
  private observers: Observer[] = [];

  public register(observer: Observer): void {
    this.observers.push(observer);
  }

  public unregister(observer: Observer): void {
    this.observers = this.observers.filter(obs => obs !== observer)
  }

  public notify(): void {
    const max = this.observers.length
    for (let i = 0; i < max; i++) {
      this.observers[i]?.notify();
    }
  }
}

export class ConcreteSubject extends Subject {
  private subjectState: unknown;

  get SubjectState(): unknown {
    return this.subjectState;
  }

  set SubjectState(subjectState: unknown) {
    this.subjectState = subjectState;
  }
}

export class Observer {
  public notify(): void {
    throw new Error("Abstract Method!");
  }
}

export class ConcreteObserver extends Observer {
  private name: string;
  private state: unknown;
  private subject: ConcreteSubject;

  constructor(subject: ConcreteSubject, name: string) {
    super();
    this.subject = subject;
    this.name = name;
  }

  public notify(): void {
    console.log(this.name, this.state);
    this.state = this.subject.SubjectState;
  }

  get Subject(): ConcreteSubject {
    return this.subject;
  }

  set Subject(subject: ConcreteSubject) {
    this.subject = subject;
  }
}

export default function SetupNotificationsScenario(): void {
  const sub: ConcreteSubject = new ConcreteSubject();

  sub.register(new ConcreteObserver(sub, "User added."));
  sub.register(new ConcreteObserver(sub, "User deleted."));
  sub.register(new ConcreteObserver(sub, "User updated"));

  sub.SubjectState = {key: "subject_state", type: "notification", error: true};
  sub.notify();
}