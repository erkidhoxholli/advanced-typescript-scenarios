import SetupDownloaderScenario from './scenarios/downloader'
import SetupNotificationsScenario from './scenarios/notifications'
import './core/core'
import './core/utilityTypes'
import './core/narrowing'
import './core/abstractClass'

// Downloader using the proxy pattern
SetupDownloaderScenario()

// Notifications using the observer pattern
SetupNotificationsScenario()