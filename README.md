#### Advanced typescript
<b>Description:</b> Learning typescript by using various design patterns and taking into account real world scenarios with self contained snippets

```
yarn
yarn dev
```

##### Scenarios:
* ```downloader.ts``` using proxy pattern (logging, caching, access/permissions)
* ```notifications.ts``` using observer pattern (for notifications)