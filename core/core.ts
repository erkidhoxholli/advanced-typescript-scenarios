import axios from "axios";

// 1. typed promise and api call
interface ApiResponse<T> {
  count: number;
  next: string;
  previous: string;
  results: T[];
}

interface Article {
  id: number;
  title: string;
  summary: string;
  image_url: string;
  published_at: Date;
  isArticle: "43243435wersdffdsfds";
}

async function getArticles(): Promise<Article[]> {
  const resp = await axios.get<ApiResponse<Article>>(
    "https://api.spaceflightnewsapi.net/v4/articles/"
  );

  return resp.data.results;
}

getArticles().then((articles) => console.log('number of articles: ', articles.length))

// 2. using as const and doing Object.values on the type level
const routes = {
  home: "/",
  auth: "/auth",
} as const;

type TypeOfRoutes = typeof routes;
type RouteKeys = keyof TypeOfRoutes; // home, auth
type Route = TypeOfRoutes[RouteKeys]; // "/", "/auth"

const goToRoute = (route: Route) => console.log(`going to route: ${route}`);
goToRoute(routes.auth);
goToRoute("/auth");

// 3. typing colors
type RGBA = `${number} ${number} ${number}`;
type Hex = `#${string}`;
type Color = RGBA | Hex;

const color1: Color = "255 255 255";
const color2: Color = "#dedede";
console.log({ color1, color2 });

// 4. utility types (Partial, Required, Omit, Pick, Readonly, NonNullable)

// 5. utility types that work with unions (Exclude, Extract)
type Shape =
  | { type: "square"; size: number }
  | { type: "circle"; radius: number }
  | { type: "triangle" };

type Role = "user" | "anonym" | "admin" | "super-admin";
type NonAdminRole = Exclude<Role, "admin" | "anonym">;
type Circle = Extract<Shape, { type: "circle" }>; // for discriminated unions

type allTypes = Role | Shape;
type OnlyRoles = Extract<allTypes, Role>;

type AdminRoles = Extract<Role, `${string}${"admin"}`>; // "admin" | "super-admin"

const nonAdmin: NonAdminRole = "super-admin"
const circle: Circle = {type: "circle", radius: 1}
const onlyRoles: OnlyRoles = "admin"
const adminRoles: AdminRoles = "super-admin"

console.log({nonAdmin, circle, onlyRoles, adminRoles})
// 6. utility types that work with functions (Awaited, ReturnType, Parameters)
function sendColor(color: Color) {
  return {
    color,
  };
}
type Data = ReturnType<typeof sendColor>;
const data: Data = { color: "#dedede" };
console.log({ data });
type Params = Parameters<typeof sendColor>;
type AwaitedArticles = Awaited<ReturnType<typeof getArticles>>;

const params: Params = [color1]
const awaitedArticles: AwaitedArticles = []
console.log({params, awaitedArticles})
// 7. Using generics
type MyGenericType<TData> = {
  data: TData;
};

type Example1 = MyGenericType<{ firstname: string }>;

const resp: Example1 = { data: { firstname: "erkid" } };
console.log(resp.data.firstname);

// 8. Generics infered types
const addIdToObject = <T>(obj: T, id: string) => {
  return {
    ...obj,
    id,
  };
};

const result = addIdToObject({ name: "Erkid" }, "1");
console.log(result.id, result.name);

// 9. using maps
const reviewMap = {
  terrible: 1,
  average: 2,
  good: 3,
  great: 4,
  incredible: 5,
} as const;

type Review = (typeof reviewMap)[keyof typeof reviewMap] | keyof typeof reviewMap;

const review1: Review = "good"
const review2: Review = 1
console.log({review1, review2})
