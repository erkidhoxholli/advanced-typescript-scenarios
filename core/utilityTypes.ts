// more here: https://www.typescriptlang.org/docs/handbook/utility-types.html
interface IUser {
  id?: number;
  firstName: string;
  lastName: string;
  createdAt?: Date;
}

// Pick
type UserFullname = Pick<IUser, "firstName" | "lastName">;
const userName: UserFullname = {
  firstName: "James",
  lastName: "Bond",
};

// Omit
type UserPost = Omit<IUser, "id" | "createdAt">;
const updateUser: UserPost = {
  firstName: "James",
  lastName: "Bond",
};

// Partial
const updateUserField = (id: number, fieldsToUpdate: Partial<UserPost>) => {
  console.info("updating: ", fieldsToUpdate, id);
};

updateUserField(1, {
  firstName: "test",
});
updateUserField(1, {
  lastName: "test",
});

// Required
const updateUserAllRequired = (user: Required<IUser>) => {
  console.info("updating: ", user);
};

updateUserAllRequired({
  id: 1,
  firstName: "test",
  lastName: "test",
  createdAt: new Date(),
});

// Record
interface Color {
  rga: string;
  name: string;
}

type ColorScheme = "primary" | "secondary";

const colorSchemes: Record<ColorScheme, Color[]> = {
  primary: [{ rga: "255 255 255", name: "white" }],
  secondary: [{ rga: "0 0 0", name: "black" }],
};