// using typeof
function getId(id: string | number | null): string {
  if (!id) return "";
  if (typeof id === "string") return id;
  return id.toString();
}

console.log(getId("1"));
console.log(getId(2));
console.log(getId(null));

// using instance of (only if we create new instance of an object)
function logValue(val: Date | string) {
  if (val instanceof Date) {
    console.log(val.getUTCDate());
    return;
  }

  console.log(val.toLocaleUpperCase());
}
logValue(new Date());
logValue("logged val");

// using in operator
type Fish = { swim: () => void };
type Bird = { fly: () => void };

function move(animal: Fish | Bird) {
  if ("swim" in animal) {
    return animal.swim();
  }

  return animal.fly();
}

move({ swim: () => console.log("swim") });
move({ fly: () => console.log("fly") });

// type predicates
function isFish(animal: Fish | Bird): animal is Fish {
  return (animal as Fish).swim !== undefined;
}

function getFood(animal: Fish | Bird) {
  if (isFish(animal)) {
    return "fish food";
  }

  return "bird food";
}

// discriminated unions (we introduce a kind/type property)
interface Laptop {
  kind: "laptop";
  brand?: string;
  battery: number;
}

interface PC {
  kind: "pc";
  brand?: string;
}

type Device = Laptop | PC;

function getBattery(device: Device): number {
  if (device.kind === "laptop") return device.battery;

  return 0;
}

console.log(getBattery({ kind: "laptop", battery: 100 }));
console.log(getBattery({ kind: "pc" }));

// exhaustive checking
function getDeviceBrand(device: Device): Device["brand"] {
  switch (device.kind) {
    case "laptop":
      return device.brand;
    case "pc":
      return device.brand;
    default:
      const _defultBrand: never = device; // this will never happen
      return _defultBrand;
  }
}

console.log(getDeviceBrand({ kind: "laptop", battery: 100, brand: "dell" }));
console.log(getDeviceBrand({ kind: "pc" }));
