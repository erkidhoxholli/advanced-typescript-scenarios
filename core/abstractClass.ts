// abstract classes are a blueprint (close to interfaces). You cant create
// objects from it such as "new TakePhoto()"" wont work
abstract class TakePhoto {
    constructor(
        public cameraMode: string,
        public filter: string
    ){
    }

    abstract getSepia(): void

    // this makes abstract classes powerful because they can be a blueprint but also have logic/calculations
    getReelTime(): number { 
        return 8
    }
}

class Instagram extends TakePhoto {
    constructor(
        public cameraMode: string,
        public filter: string,
        public burst: number
    ) {
        super(cameraMode, filter) // note: we need to use super 
    }

    getSepia(): void {
        console.log('getSepia()')
    }

    getReelTime(): number {
        return 100
    }
}

const erkidInsta = new Instagram("", "", 1) // !! but we can create objects from the class that inherits the abstract class
console.log(erkidInsta.getReelTime())